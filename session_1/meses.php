<?php
  $meses = array('Enero','Febrero','Marzo','Abril',
  'Mayo','Junio','Julio','Agosto',
  'Septiembre','Octubre','Noviembre','Diciembre');
//echo count($meses);
//sort($meses);
rsort ($meses);
 ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Meses del Año</title>
  </head>
  <body>
    <ul>
      <?php
foreach ($meses as $mes) {
  echo '<li>'. $mes. '</li>';
}
       ?>
    </ul>
  </body>
</html>
